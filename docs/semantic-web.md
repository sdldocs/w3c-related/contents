# Semantic Web <sup>[1](#footnote_1)</sup>

Semantic Web은 웹페이지, 어플리케이션, 파일 간에 데이터를 연결하기 위한 비전이다. 웹 1.0은 링크된 웹페이지, 웹 2.0은 링크된 앱, 웹 3.0은 링크된 데이터에 관한 것으로 웹의 자연스러운 진화의 일부로 간주하는 사람들도 있다. 이는 컴퓨터 과학자 Tim Berners-Lee의 World Wide Web에 대한 원래 계획의 일부였지만 당시에는 대규모로 구현하기에는 실용적이지 않았다.

언젠가는 모든 데이터가 하나의 Semantic Web으로 연결될 것이라는 원대한 비전이 있다. 실제로 오늘날의 Semantic Web은 [검색 엔진 최적화(SEO)](https://www.techtarget.com/whatis/definition/search-engine-optimization-SEO), 비즈니스 지식 관리, 제어된 데이터 공유 등 전문화된 용도에 따라 세분화되어 있다.

SEO에서는 이제 모든 주요 검색 엔진이 제품, 책, 영화, 레시피, 비즈니스와 같은 일반적인 범주의 엔터티에 대한 전문 스키마를 사용하여 정보를 연결하기 위한 Semantic Web 기능을 지원한다. 이러한 스키마는 Google 검색 결과에 표시되는 요약을 생성하는 데 도움이 되고 있다.

비즈니스 지식 관리의 경우, 기업은 다양한 도구를 사용하여 기업 문서, 비즈니스 서비스 및 공개 웹에서 스크랩한 정보로부터 엔티티와 그 관계를 그래픽으로 표현한 [시맨틱 네트워크(지식 그래프)](https://www.techtarget.com/searchcontentmanagement/definition/semantic-network-knowledge-graph)를 큐레이션할 수 있다. 이를 통해 조직의 계획, 분석과 협업을 개선할 수 있다.

통제된 데이터 공유는 더 열망적이고 실험적인 개념이다. 핵심 아이디어는 개인과 기업이 자신과 관심사에 대한 안전한 데이터 저장소를 만든 다음 이 데이터에 대한 링크를 기업, 의사 또는 정부 기관과 같은 신뢰할 수 있는 당사자와 공유할 수 있다는 것이다.

## Semantic Web 프레임워크의 요소
Berners-Lee는 Semantic Web을 구현하기 위해 함께 사용해야 하는 다양한 종류의 도구와 작업을 시각화하는 데 도움이 되는 Semantic Web 스택이라는 그림 또는 모델을 제안했다. 이 스택은 개발자가 단순히 다른 웹페이지에 연결하는 것에서 나아가 웹페이지, 문서, 어플리케이션 및 데이터 소스 전반에서 데이터와 정보를 연결하는 방법을 모색하는 데 도움을 준다.

![](./images/semantic_web_layer_cake-h.png)

다음은 위 스택을 설명한다.

- 스택의 맨 아래에서 원시 데이터는 유니코드 텍스트 문자를 사용하여 표현할 수 있다. 유니폼 리소스 식별자(URI)는 특정 페이지 내의 데이터에 대한 링크를 제공할 수 있다.
- 다음으로, XML은 종종 기계가 읽을 수 있는 형식으로 페이지의 정보를 구조화하는 데 사용된다.
- 그 위 [RDF](https://sdldocs.gitlab.io/w3c-related/rdf/)는 데이터 교환을 위해 엔티티, 속성과 이들 간의 관계를 설명하는 표준 방법을 제공한다.
- [OWL](https://sdldocs.gitlab.io/w3c-related/owl-primer/)은 엔티티에 대한 지식과 엔티티 간의 지식을 표현하는 방법을 공식화한다. 이 언어는 W3C의 규칙 교환 형식(RIF)과 함께 작동하여 공식화하기 어려운 것들을 설명한다.
- [SPARQL]([SPARQL](https://sdldocs.gitlab.io/w3c-related/sparql/)) 쿼리 언어는 다양한 소스에 저장된 데이터를 검색할 수 있으며 OWL과 RDF와 함께 작동하여 정보를 찾을 수 있다.
- 다른 기술도 이러한 핵심 시맨틱 처리 서비스와 함께 작동하여 데이터를 보호하고, 감사 추적을 생성하여 신뢰를 강화하고, 사용자 경험을 제공해야 한다.

## 잠재적인 Semantic Web의 용도
Semantic Web의 실제와 잠재적 응용 분야는 다음과 같다.

**SEO**는 오늘날 가장 일반적으로 사용되고 있다. 웹사이트 소유자나 콘텐츠 제작자가 표준 검색 엔진 스키마에 따라 링크된 데이터 태그를 추가하면 검색 엔진이 매장 시간, 제품 유형, 주소, 타사 리뷰 등의 데이터를 자동으로 추출하기가 더 쉬워진다. Rotten Tomatoes 웹사이트는 구조화된 데이터를 추가한 후 클릭률이 25% 향상되었다.

**자동 요약**. 웹사이트와 타사 앱은 태그가 지정된 데이터를 사용하여 다양한 사이트에서 특정 타입의 정보를 자동으로 요약 카드로 가져올 수 있다. 예를 들어, 영화관에서는 상영 시간, 영화 리뷰, 극장 위치와 검색에 표시되는 할인 가격을 나열할 수 있다.

GS1 웹 어휘를 사용하여 공급망 전반에서 제품 **세부 정보를 공유**한다. 이를 통해 제조업체와 도매업체는 식품, 음료와 기타 소비재에 대한 정보를 컴퓨터에서 액세스할 수 있는 방식으로 자동으로 전송할 수 있다. 이를 통해 웹사이트는 제품을 판매할 수 있는 식료품점과 온라인 상점에 영양 성분 라벨, 크기, 알레르기 정보, 수상 경력, 유통기한, 구매 가능 날짜 등을 더 쉽게 나열할 수 있다.

**기술 분류 체계 표준화**. 학습 플랫폼, 채용 웹사이트, 인사팀은 모두 직무 기술을 설명하는 데 서로 다른 용어를 사용할 수 있다. 점점 더 많은 기업이 Semantic Web 기술을 사용하여 기술을 설명하는 다양한 방식을 [표준 분류 체계](https://www.techtarget.com/searchhrsoftware/feature/New-enterprise-learning-tools-put-employees-in-control)로 변환하고 있다. 이를 통해 팀은 지원자 검색 범위를 넓히고 직원을 위해 개발하는 교육 프로그램을 개선할 수 있다.

**제어된 데이터 액세스 제공**. 소비자는 종종 수십 개의 다른 회사에 이름, 주소, 주민등록번호, 선호 사항 등 동일한 정보가 포함된 수십 개의 양식을 작성한다. 이러한 조직이 침해당하면 데이터가 손실된다. 이러한 문제를 해결하기 위해 Berners-Lee의 회사인 Inrupt는 다양한 커뮤니티, 병원 및 정부와 협력하여 소비자가 데이터에 대한 액세스 권한을 공유할 수 있는 Solid 오픈 소스 프로토콜을 기반으로 구축된 보안 데이터 포드를 출시하고 있다.

**디지털 트윈 데이터 공유**. 벤틀리, 지멘스 등 여러 벤더가 산업 메타버스라고 부르는 산업과 인프라를 위한 커넥티드 Semantic Web을 개발하고 있다. 이러한 차세대 디지털 트윈 플랫폼은 산업별 온톨로지, 제어된 액세스 및 데이터 연결을 결합하여 사용자가 다양한 어플리케이션과 관점에서 건물, 도로와 공장에 대한 동일한 데이터를 보고 편집할 수 있도록 한다.

![](./images/semantic_web_timeline-f.png)

## Semantic Web은 Web 3.0과 어떤 관련이 있나? 
Semantic Web은 종종 Web 3.0이라고 불린다. Berners-Lee는 1989년부터 시작된 World Wide Web 작업 초기에 Semantic Web과 같은 개념을 설명하기 시작했다. 당시 그는 연결된 데이터를 생성, 편집과 보기를 위한 정교한 어플리케이션을 개발하고 있었다. 하지만 이 모든 작업에는 고가의 NeXT 워크스테이션이 필요했고, 소프트웨어는 대량 소비를 위한 준비가 되어 있지 않았다.

모자이크 브라우저의 인기는 웹 형식에 대한 열정과 지원의 임계점을 구축하는 데 도움이 되었다. 이후 브라우저 기반 프로그래밍의 표준이 된 JavaScript로 프로그래밍 가능한 콘텐츠가 개발되면서 콘텐츠 제작과 인터랙티브 앱의 기회를 열렸다.

이후 O'Reilly Media 미디어의 창립자이자 CEO인 Tim O'Reilly는 동명의 컨퍼런스를 통해 Web 2.0이라는 용어를 대중화했다. 그러나 Web 2.0은 여전히 Semantic Web의 정의 기능인 페이지의 데이터를 설명하는 방법을 공식화하지 못했다. 한편, Berners-Lee는 W3C에서 일하면서 데이터 연결에 대한 탐구를 계속했다.

이후 Semantic Web을 Web 3.0이라고 부르는 관습이 영향력 있는 관찰자들 사이에서 자리 잡기 시작했다. 2006년 저널리스트 John Markoff는 *New York Times*에 Semantic Web을 기반으로 구축된 Web 3.0이 인터넷의 미래를 대표한다고 썼다. 2007년 미래학자이자 발명가인 Nova Spivak은 Web 2.0이 집단 지성에 관한 것이라면, 새로운 Web 3.0은 연결 지성에 관한 것이라고 제안했다. Spivak은 Web 3.0이 데이터 웹에서 시작하여 향후 10년 동안 본격적인 Semantic Web으로 진화할 것이라고 예측했다.

Gavin Wood는 2014년에 블록체인을 기반으로 하는 탈중앙화된 온라인 생태계를 설명하기 위해 Web3라는 용어를 만들었습니다. Berners-Lee의 선구적인 작업을 이어온 Inrupt는 Semantic Web이 Web3라는 용어와는 다른 Web 3.0을 구축하는 것이라고 주장하였다. 논쟁의 핵심은 Web3가 블록체인에 초점을 맞추면서 상당한 오버헤드가 추가된다는 것이다. 이와 대조적으로 Inrupt의 접근 방식은 데이터 소유자가 제어하는 안전한 중앙 집중식 스토리지에 중점을 두어 ID와 액세스 제어를 시행하고 어플리케이션 상호 운용성을 간소화하며 데이터 거버넌스를 보장한다. 지지자들은 이러한 메커니즘이 Semantic Web이 더 나은 검색을 위한 플랫폼에서 더 연결된 신뢰할 수 있는 데이터의 웹으로 진화하는 데 필요한 요소를 추가한다고 주장한다.

> **Note**
>
> 웹 3.0에 대한 자세한 내용은 다음 문서를 참조하시오.
>
> - [10 core features of Web 3.0 technology](https://www.techtarget.com/searchcio/tip/10-core-features-of-Web-30-technology)
> - [The biggest advantages and disadvantages of Web 3.0](https://www.techtarget.com/searchcio/tip/The-biggest-advantages-and-disadvantages-of-Web-3)
> - [The 10 most promising tools for Web 3.0 development](https://www.techtarget.com/searchcio/tip/The-most-promising-tools-for-Web-30-development)
> - [Web 3.0 security risks: What you need to know](https://www.techtarget.com/searchsecurity/tip/Top-3-Web3-security-and-business-risks)
> - [8 top Web 3.0 use cases and examples](https://www.techtarget.com/searchcio/tip/8-top-Web-30-use-cases-and-examples)
> - [5 ways Web 3.0 will impact digital marketing](https://www.techtarget.com/searchcio/tip/5-ways-Web-30-will-impact-digital-marketing)

## Semantic Web의 한계와 비판
1세대 Semantic Web 도구는 온톨로지와 지식 표현에 대한 깊은 전문성을 필요로 했다. 따라서 주로 웹사이트에 더 나은 메타데이터를 추가하여 페이지에 있는 내용을 설명하는 데 사용되었다. 페이지를 추가하거나 변경할 때 메타데이터를 입력하는 추가 단계가 필요했다. CMS가 점점 더 좋아지고 있다.

하지만 이는 SEO의 과제를 단순화할 뿐이다. 여러 사이트의 데이터를 결합하기 위한 보다 정교한 Semantic Web 어플리케이션을 구축하는 것은 데이터를 설명하기 위해 서로 다른 스키마를 사용하고 개인이 세상을 설명하는 방식에 창의적인 차이가 있기 때문에 여전히 어려운 문제이다.

문장의 주어, 술어, 목적어를 식별하기 위한 시맨틱 분석은 영어 학습에 유용하지만, 다른 사람들이 작성한 문장을 분석할 때 항상 일관성이 있는 것은 아니며, 이는 매우 다양할 수 있다. 다른 의미, 때로는 모순되는 의미를 가질 수 있는 인기 있는 유행어의 경우 상황이 더 복잡해질 수 있다. 예를 들어, 과학자들은 모두 양자 도약이 원자가 만들 수 있는 가장 작은 에너지 변화라는 데 동의하지만, 마케터들은 모두 양자 도약이 꽤 큰 변화라고 생각하는 것 같다.

또 다른 주요 과제는 Semantic Web에 표현된 데이터에 대한 신뢰를 구축하는 것이다. 페이지에 기록된 내용뿐만 아니라 누가 그것을 말했는지, 그리고 그들의 편견이 무엇인지 아는 것이 점점 더 중요해지고 있다. 컨슈머 리포트와 같이 공신력 있는 사이트의 추천과 아마존의 SpamBob432의 추천은 그 무게가 다를 수 있다. Semantic Web에 대한 감사 추적을 제공하려는 노력은 데이터를 연결할 뿐만 아니라 데이터 품질과 신뢰도 수준을 이해하는 데도 도움이 될 수 있다.

## Semantic Web의 미래
검색 엔진 결과를 향상시키기 위해 페이지에 시맨틱 데이터를 자동으로 추가하는 웹사이트가 점점 더 많아지고 있다. 하지만 사물에 대한 데이터가 웹 페이지 전체에 완전히 연결되기까지는 아직 갈 길이 멀다. 다양한 어플리케이션에서 데이터의 의미를 번역하는 것은 해결해야 할 복잡한 문제이다.

AI와 자연어 처리의 혁신은 특히 기술 분류, 계약 인텔리전스 또는 디지털 트윈 구축과 같은 특정 영역에서 이러한 격차를 해소하는 데 도움이 될 수 있다. 앞으로는 조직이나 업계에서 데이터를 설명하는 데 사용하는 스키마에 대한 더 나은 거버넌스와 AI 및 통계 기법을 결합하여 격차를 메우는 하이브리드 접근 방식이 점점 더 많이 사용될 수 있다. 연결된 데이터 웹이라는 원래의 비전에 더 가까워지려면 더 나은 구조, 더 나은 도구, 신뢰의 사슬이 결합되어야 할 것이다.


<a name="footnote_1">1</a>: 이 페이지는 [Semantic Web](https://www.techtarget.com/searchcio/definition/Semantic-Web)을 편역한 것임.
