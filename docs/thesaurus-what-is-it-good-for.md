# 동의어 사전이란 무엇이며 어떤 용도로 사용되나? <sup>[1](#footnote_1)</sup>
제어 어휘(controlled vocaburary)와 지식 조직 시스템(knowledge organization system) 영역에서 "제어 어휘", "분류체계(taxonimy)", "시소러스", "온톨로지" 및 "지식 그래프"에 대해 서로 다른 의미가 계속 존재한다는 것은 다소 아이러니하다. [분류체제와 분류(Taxonomies vs Classification)](./taxonomies-vs-classification.md), 분류체제와 탐색, 분류체제는 계층적이어서는 안 되는 경우에 대한 이전 포스팅에서 분류가 무엇이고 그렇지 않은지에 대해 어느 정도 설명되었기를 바란다. 이제 시소러스로 넘어가 보겠다.

## 디소러스의 다른 의미
최근에 분류체제, 온톨로지와 지식 그래프에 관한 웨비나에 참석했는데, 이 웨비나에서는 시소러스를 목록에서 식별된 각 개념에 대한 동의어 집합으로 설명했다. 이것은 이 맥락에 맞는 정의가 아니다. 각 개념 목록에 대한 동의어 집합을 분류학자(taxonomists)들은 "동의어 링"이라고 부르며, 엔터프라이즈 검색 엔진의 관리자는 "검색 시소러스"라고 부른다. 이 경우 "시소러스"라는 단어의 사용은 각 단어에 동의어가 표시되는 Roget's 시소러스와 같은 사전형 시소러스(Wikipedia의 기본 시소러스 항목)를 의미한다. 사용자가 검색창에 입력한 잠재적인 단어, 구를 콘텐츠 텍스트에 나타날 가능성이 있는 단어나 구와 일치시켜 검색자가 다른 동의어를 사용함으로써 콘텐츠를 놓치는 일이 없도록 검색을 지원하기 위해 동의어가 포함된다.

그러나 "검색 시소러스"(동의어 링)는 사용 용도가 다르기 때문에 동의어 사전 시소러스와 몇 가지 점에서 차이가 있다.

- 검색 시소러스는 사전 시소러스처럼 단일 단어가 아닌 구문을 포함한다.
- 검색 시소러스는 사전처럼 품사만 포함하는 것이 아니라 명사, 동명사(?) 또는 명사구인 개념으로 구성된다.
- 검색 시소러스의 '동의어'는 사전에서 제시하는 것처럼 일부 경우에만 사용될 수 있는 동의어가 아니라 콘텐츠 리포지토리의 모든 경우에 상호 교환적으로 사용될 수 있는 적절하게 동등한 용어이다.

그러나 (검색 관리의 맥락이 아닌) 분류체제/온톨로지의 맥락에서 시소러스라는 명칭은 상당히 다른 의미를 갖는다. 정보 시소러스 또는 정보 검색 시소러스(동의어 사전 유형과 구별하기 위해)라고도 하는 [시소러스(정보 검색)]()에 대한 Wikipedia의 다른 항목에는 "콘텐츠 객체의 색인에서 메타데이터의 의미적 표현을 지시하려는 통제된 어휘의 형태"로 정의하고 있다. 이는 분류체계와 온톨로지와 관련된 의미이다. Wikipedia 정의보다 더 중요한 것은 시소러스를 구성하는 방법에 대해 발표된 표준/가이드라인인 [ISO 25964 Thesauri and interoperability with other vocabularies]()와 [ANSI/NISO Z39.19-2005 (R2010) Guidelines for the Construction, Format, and Management of Monolingual Controlled Vocabularies]()이다. 후자는 제목에 시소러스라는 이름을 붙이지 않았지만(이전 버전에서는 그랬지만) 본질적으로 시소러스에 관한 것으로, 4.1 정의 섹션에서 시소러스를 "알려진 순서로 배열되고 용어 간의 다양한 관계가 명확하게 표시되고 표준화된 관계 지표에 의해 식별되도록 구조화된 제어 어휘"라고 정의하고 있다.

따라서 시소러스는 명사구인 용어, 용어 간의 계층적 관계, 용어 간의 연관성(계층적 관계는 아니지만 관련성이 있는) 관계, 비선호 용어인 '동의어' 또는 변형어, 용어에 대한 범위 메모 등 상당히 구조화되고 일정한 표준 기능을 갖춘 일종의 통제된 어휘 또는 일종의 지식 조직 시스템이다. 용어에 대한 다른 메타데이터도 가능하며, 계층적 관계와 연관 관계의 변형도 가능하다.

## 시소러스의 유용성
제어 어휘(지식 조직 시스템) 타입의 연속체 차트에서 시소러스는 복잡성과 의미론 지원 수준에서 분류법과 온톨로지 사이에 속한다.

![](./images/controlled-vocabulary-types-chart-thesauri.png)

분류체제와 온톨로지는 모두 유용한 것으로 인정받기 때문에 그 사이에 있는 것을 유용한 것으로 간주하지 않는 것은 비논리적으로 보일 수 있다. 시소러스는 온톨로지만큼 복잡하지 않으면서도 분류체제보다 더 많은 의미를 지원한다는 장점이 있다.

대부분의 관계가 계층적이라 하더라도 프로세스와 에이전트, 행위와 속성, 원인과 결과, 대상과 출처, 분야와 실무자 등과 같이 관련 주제 간의 연관 관계를 만드는 것이 논리적으로 보이고 사용자에게 도움이 될 때가 있을 수 있다. 또는 주제가 아닐 수도 있다. 예를 들어 이커머스에서는 '관련' 제품 카테고리를 추천하거나 활동 콘텐츠에서 활동과 제품을 연관시킬 수 있다. 전문가 인물 찾기에서 사람 이름을 전문 분야와 관련시킬 수 있으며, '관련' 타입의 범위를 제한적으로 유지하면 여러 타입으로 정의된 의미 관계가 있는 온톨로지의 복잡성 수준까지 가지 않고도 일반적인(generic) 연관 관계("관련 용어")로 충분할 수 있다.

추가된 연관 관계와 동의어/비선호 용어의 포괄적인 포함은 인덱서에게 제안을 제공하거나 자동 분류 도구에 컨텍스트를 제공함으로써 수동이든 자동이든 더 나은(보다 포괄적인) 태깅을 지원한다.

마지막으로, 시소러스의 전체 구조는 분류 체계보다 더 유연하다. 분류는 개념을 제한된 수의 상위 개념(또는 "상위 용어")이 있는 카테고리로 그룹화한다. 더 넓은 개념 관계도 없고 더 좁은 개념 관계도 없는 개념을 "고아(orphan)"라고도 하는데, 이는 분류체계에서는 오류로 간주된다. 반면에 시소러스에서는 상위 계층 구조가 필요하지 않고(존재할 수는 있지만) 연관 관계가 포함되는 경우, 더 넓은 관계도 없고 더 좁은 관계도 없지만 최소한 연관 관계만 있는 개념을 허용한다. 따라서 분류학자가 항상 이상적이지 않을 수 있는 기존 계층 구조에 새로운 개념을 억지로 집어넣을 필요는 없다.

## 시소러스 관리를 위한 소프트웨어
시소러스의 개발과 유지 관리를 지원하는 소프트웨어도 한동안 사용 가능했다. ([Taxobank](http://www.taxobank.org/)에는 2013년 이후 업데이트되지 않은 과거 목록이 있다.) 분류체제를 만드는 데 사용되는 소프트웨어는 실제로는 '시소러스' 관리 소프트웨어이며, 연관 관계와 같은 추가된 시소러스 기능은 단순한 분류체제를 만들 때 활용되지 않기 때문에 "분류체제" 관리 소프트웨어라는 것은 실제로 존재하지 않는다.

시소러스보다 분류체제가 더 많이 사용됨에 따라 소프트웨어 공급업체는 알파벳순이 아닌 계층적 표시를 기본으로 하고, 분체체제와 온톨로지를 위한 솔루션을 마케팅하고 시소러스에 대한 언급을 강조하지 않거나 생략함으로써 이를 반영하고 있다. 예를 들어, [PoolParty Semantic  제품군](https://www.poolparty.biz/product-overview)의 기본 핵심 모듈은 시소러스를 쉽게 만들 수 있기 때문에 Thesaurus Server라는 이름이 적절하지만, 기본 계층적 디스플레이에서 분류체제를 사용할 수 있도록 제안하고 웹사이트의 제품 페이지에는 "Enterprise Taxonomy and Ontology Management"용이라고 명시되어 있다.

## 시소라스의 현재
시소러스 설계 원칙은 시소러스와 분류체제 모두에 적용 가능하다. 따라서 시소러스는 정보 아키텍처 과정을 포함하여 도서관 과학과 정보 과학 학위 프로그램에서 계속 가르치고 있다. *Information Architecture for the Web and Beyond*(Rosenfeld, Morville, 아랑고)라는 책(표지 디자인 때문에 북극곰 책이라고도 함)은 2015년 4판에서도 "Thesauri, Controlled Vocabularies and Metadata" 장의 거의 절반에 해당하는 20페이지를 시소러스에 할애하고 있다.

시소러리를 사용하는 데 있어 가장 큰 장애물은 오늘날 가장 일반적으로 구현되는 상용 콘텐츠 관리 시스템(CMS)가 일반적으로 시소러스의 기능을 지원하지 않는다는 것이다. 연관 관계는 거의 지원되지 않는다. 동의어/비선호 용어는 부분적으로만 지원될 수 있다 (예: 태그 보기에서는 지원되지만 검색에서는 지원되지 않음). 따라서 시소러스는 정보 검색 데이터베이스 게시자의 시스템과 같은 맞춤형(자체 개발) 사용자 시스템에서만 구현되는 경향이 있다.

정보 검색 시소러스는 오랫동안 사용되어 왔으며, 오늘날 비즈니스와 산업계에서 시소러스를 받아들이는 데 있어 문제의 일부가 될 수도 있다. 사람들은 시소러스를 디지털 시스템이 아닌 인쇄 시스템만 사용하던 시절에 주로 사용되던 일종의 레거시 지식 조직 시스템으로 간주할 수 있다. 시소러스가 인쇄물에서 유용하도록 설계된 것은 사실이지만, 시소러리의 설계는 디지털 구현에도 적용 가능하고 관련성이 있다. 또한 시소러스는 상호 연결된 제어 어휘로 구성된 더 큰 시스템의 일부를 구성할 수도 있다.

이제 시소러스와 연결될 수 있는 다음 주제인 온톨로지에 대해 알아보겠다. 다음 [게시물](./differing-definitions-of-ontologies.md)에서는 온톨로지의 다양한 의미에 대해 다룰 것이다.


<a name="footnote_1">1</a>: 이 페이지는 [What is a Thesaurus and What is it Good For](https://www.hedden-information.com/what-is-a-thesaurus-and-what-is-it-good-for/)을 편역한 것임.
