# 목차

## W3C 관련 Article 모음

- [OWL primer](https://suredata.gitlab.io/dcat-ap-knpp/w3c-related/owl-primer/)
- [RDF](https://suredata.gitlab.io/dcat-ap-knpp/w3c-related/rdf/)
- [SKOS](https://suredata.gitlab.io/dcat-ap-knpp/w3c-related/skos/)
- [SPARQL](https://suredata.gitlab.io/dcat-ap-knpp/w3c-related/sparql/)

## Unclassified

### Under Working
- [Semantic Web](./semantic-web.md)
- [Web 3.0 (Web3)이란? 정의, 지침 그리고 역사](./web-30.md)
- [Taxonomies vs. Ontologies](./taxonomies-vs-ontologies.md)
- [Taxonomies vs. Classification](./taxonomies-vs-classification.md)
- [동의어 사전이란 무엇이며 어떤 용도로 사용되나?](./thesaurus-what-is-it-good-for.md)
- [온톨로지에 대한 다양한 정의](./differing-definitions-of-ontologies.md)
- [아마 OWL은 불필요할 껄 ](./dont-need-owl.md)
- [SKOS로 분류체제 관리](./skosibm.md)
- [지식 그래프!](./knowledgegraph.md)

### To Read
- [Web 2.0 vs. Web 3.0 vs. Web 1.0: What's the difference?](https://www.techtarget.com/whatis/feature/Web-20-vs-Web-30-Whats-the-difference)
- [RDF and SPARQL: Using Semantic Web Technology to Integrate the World's Data](https://www.w3.org/2007/03/VLDB/)
- [Using OWL and SKOS](https://www.w3.org/2006/07/SWD/SKOS/skos-and-owl/master.html)
- [Validating RDF data with SHACL](https://www.bobdc.com/blog/validating-rdf-data-with-shacl/)
- [지식 그래프](http://www.bobdc.com/blog/knowledgegraphs/)
- [the leading introduction to taxonomy development](https://www.hedden-information.com/accidental-taxonomist/)

