# 아마 OWL은 불필요할 껄 <sup>[1](#footnote_1)</sup>

최근 블로그 게시물인 [RDF란 무엇인가?](https://www.bobdc.com/blog/whatisrdf/), [RDFS란 무엇인가?](https://www.bobdc.com/blog/whatisrdfs/), [RDFS로 무엇을 할 수 있나?](https://www.bobdc.com/blog/whatisrdfspart2/), [SKOS로 분류체제 관리](https://www.bobdc.com/blog/skosibm/)하기에서 일부 독자들이 "OWL이란 무엇인가?"에 대한 후속 글을 작성해 주었으면 좋겠다고 문의했다. 한 질문자에게는 [Learning SPARQL](http://www.learningsparql.com/)의 39~41페이지와 263~269페이지를 읽어보라고 권하였는데, 이 책이 OWL의 역사와 원래 의도의 중요한 부분인 집합 기반 논리를 수행하는 방법에 대해 꽤 잘 소개했다고 생각한다.

저자의 전 직장인 TopQuadrant의 창업자인 Irene Polikoff가 최근 블로그에 올린 글도 사람들이 언제 OWL을 사용해야 하는지, 사용하지 말아야 하는지에 대한 [많은 대화](https://twitter.com/jindrichmynarz/status/1401918042532110349)를 불러 일으켰다. 이 블로그의 제목은 [Why I Don’t Use OWL Anymore](https://www.topquadrant.com/owl-blog/)라는 꽤 단정적인 제목이다. 자는 OWL의 일부가 그녀보다 더 유용할 수 있다고 생각하지만, 여전히 많은 사람들에 비해 덜 유용하다고 생각한다. 아래에서 몇 가지 예를 들어보겠다.

## 데이터 모델링? RDFS 사용
가장 간단한 수준에서 데이터 모델링은 추적하고자 하는 정보 조각과 이들 간의 관계를 식별하고 열거하는 것이다. 여기에 표준 기반, 기계 판독 가능 버전은 어플리케이션 개발에 매우 유용하다. [RDFS란 무엇인가?](https://www.bobdc.com/blog/whatisrdfs/)와 [RDFS로 무엇을 할 수 있나?](https://www.bobdc.com/blog/whatisrdfspart2/)에서 설명했듯이, RDFS는 이러한 기능을 매우 잘 수행할 수 있다. 이 두 글 중 첫 번째 글에서 [설명한 것](https://www.bobdc.com/blog/whatisrdfs#schemaorg)처럼 RDF 기반 기술의 훌륭한 성공 사례 중 하나인 [schema.org](https://schema.org/)에 특히 효과적이다. RDFS를 넘어 데이터의 구조와 잠재적 관계에 대한 정보를 훨씬 더 자세히 추가할 수 있다, 그러나 앞으로 살펴보겠지만 이러한 정보를 기계가 읽을 수 있는 설명은 이러한 설명을 읽고 어플리케이션에 가치를 제공하는 데 사용할 수 있는 도구가 없다면 별 소용이 없다.

## 데이터 모델에 제약 조건을 정의할 수 있을까? SHACL 사용
OWL은 RDFS를 넘어 클래스와 속성에 대한 추가 세부 정보를 설명할 수 있지만, 클래스의 유효한 인스턴스로 간주되는 것과 그렇지 않은 것을 설명할 수 있는 경우는 거의 없다. 이는 사람들이 컴퓨터에서 데이터를 사용해 온 오랜 기간 동안 데이터 처리의 근본적인 필요성이었다. 데이터를 사용하는 어플리케이션을 작성하는 개발자는 자신이 읽은 데이터가 실제로 예상한 것과 일치하는지 확인하기 위해 많은 코드를 작성하고 싶지 않다. 데이터를 생성한 프로세스에서 이미 이러한 유효성 검사를 수행했다고 가정하고 싶어한다. SQL의 CREATE TABLE 문을 사용하면 테이블 열의 데이터 타입과 테이블 열 간의 종속성을 지정할 수 있으며, 어떤 것이 필수이고 어떤 것이 선택적인지는 말할 것도 없고, DTD와 이후 스키마 형식도 XML에 대해 동일한 기능을 수행한다.

[SHACL로 RDF 데이터 유효성 검사하기](https://www.bobdc.com/blog/validating-rdf-data-with-shacl/)에서 설명한 것처럼, W3C 표준 SHACL이 나오기 전까지는 RDF에 이러한 기능이 없었다. 위에서 언급한 블로그 글에 대한 Irene의 후속 글은 [온톨로지 모델 정의에 SHACL을 사용하는 이유](https://www.topquadrant.com/shacl-blog/)라는 제목으로 SHACL이 가져다주는 많은 이점을 설명한다. (그녀는 "나는 (클래스와 서브클래스를 선언하는 것 외에는) 더 이상 RDFS/OWL을 사용하지 않는다"고 쓰고 있어, RDFS 사용을 완전히 대체하지는 않았다.)

## 통제 어휘? SKOS 사용
지난달 [SKOS를 사용한 분류 체제 관리](https://www.bobdc.com/blog/skosibm/)에서 분류 체제와 시소러스가 일반적으로 서로의 관계를 포함하여 어휘 용어에 대한 메타데이터를 저장할 수 있도록 해주는 제어 어휘집이라는 점을 설명한 적이 있다. 분류 체제나 시소러스는 트리에 배열된 용어의 잠재적으로 큰 모음으로, 트리의 하위 레벨이 상위 레벨의 하위 집합을 설명하는 것으로 상상할 수 있다. 이 모든 것을 RDF로 표현하려면 OWL 클래스로 해야 할까? 저자는 아니라고 한다. 이는 망치로 못을 박는 것이 아니다.

우선, 분류 트리의 하위 레벨은 상위 레벨의 하위 집합을 나타내지 않는다. 트리의 노드는 사물의 집합이 아니라 용어를 나타내며, 트리의 하위 수준은 "dog"의 보다 구체적인 버전인 "collie"와 "bulldog", "mammal"의 보다 구체적인 버전인 "dog"와 같이 보다 구체적인 용어를 보여준다. [the leading introduction to taxonomy development](https://www.hedden-information.com/accidental-taxonomist/)의 저자인 Heather Hedden은 블로그 게시물 [온톨로지에 대한 다양한 정의](./differing-definitions-of-ontologies.md)에서 이를 잘 요약했다. "온톨로지 구조는 데이터를 모델링하기 위한 것이지, 일반(보통명사)이거나 엔티티(고유명사)로 명명된 분류 개념을 조직하기 위한 것이 아니다."

분류체제에서 "Employee보다 Person이 광범위하다."은 직원에 관한 책이나 기타 형태의 미디어도 사람에 관한 저작물임을 의미한다. 온톨로지에서 "Employee는 Person의 하위 클래스"는 모든 사람에게 적용되는 속성(성, 이름)과 직원에게는 적용되지만 사람에게는 적용되지 않는 속성(채용 날짜, 급여)을 구분할 수 있게 해준다.

SKOS는 그 자체로 통제 어휘와 메타데이터를 저장하기 위한 데이터 모델을 정의하는 OWL 온톨로지이다. 널리 사용되는 어휘 관리 도구 중 상용과 오픈 소스가 이를 지원한다. (Pinterest는 분류 체제 관리를 위해 [자체 온톨로지](https://arxiv.org/pdf/1907.02106.pdf)를 개발했지만, SKOS를 기반으로 하였다.) SKOS는 이 특정 업무에 특화된 W3C 표준이다. SKOS 어휘와 OWL 온톨로지는 서로를 입력으로 사용할 수 있으며, 간단한 SPARQL 쿼리로 둘 중 하나를 생성할 수 있지만 서로 다른 목적임을 염두에 두어야 한다. 수년 동안 SKOS 기반 도구가 달성한 견인력은 어휘 관리에 이 표준을 사용해야 한다는 강력한 논거이도 하다.

## 하지만 OWL이 정말 필요하다면...
OWL이 정말 필요하다면 증명해 보세요! 데이터와 OWL 프로세서를 사용해 이 프로세서가 없었다면 눈에 띄게 더 어려웠을 작업을 수행해 보세요. 이를 통해 OWL이 데이터에 어떤 가치를 가져다주는지 입증할 수 있다.

예를 들어, [블레이즈그래프(OWL의 일부만 지원)](https://www.bobdc.com/blog/trying-out-blazegraph/)를 사용해 본 결과, 다양한 의자와 책상이 여러 방에 있다는 트리플과 어떤 방이 어떤 건물에 있다는 트리플이 있지만 어떤 가구가 어떤 건물에 있다는 트리플(또는 가구로 간주되는 것)은 없는 데이터 집합을 보여줬다. 그런 다음 RDFS `rdfs:subClassOf` 속성을 사용하여 `dm:Chair`와 `dm:Desk`가 `dm:Furniture`의 하위 클래스임을 선언하고, `dm:locatedIn` 속성이 `owl:TransitiveProperty`임을 선언했다. 이러한 추가 모델링 트리플을 사용하면 `rdfs:subClassOf`와 o`wl:TransitiveProperty`를 이해하는 OWL 프로세서에 대한 SPARQL 쿼리를 통해 어떤 가구가 어떤 건물에 있는지 나열할 수 있다. 이 작은 OWL은 실제로 모델에 약간의 의미도 추가했는데, 이는 `dm:locatedIn`의 '의미'에 대해 우리에게 그리고 OWL 프로세서에 약간의 정보를 제공하기 때문이다.

꽤 쉬웠습니다. 특정 기술의 가치를 입증하고 싶다면, 그 기술이 없었다면 불가능하지는 않더라도 훨씬 더 힘들었을 일을 그 기술로 할 수 있다는 것을 보여주는 것이 좋은 일반적인 규칙이라고 생각한다. 직원 또는 시설 데이터와 같이 다양한 비즈니스와 관련된 데이터에 대한 쿼리는 이를 위한 좋은 방법이다. (저자는 항상 Protégé의 유명한 [pizza 온톨로지https://protegeproject.github.io/protege/getting-started/#open-the-pizza-ontology]()가 데모 도메인으로 너무 귀엽다고 생각했다. 물론 누구나 피자를 좋아하지만, 사람들이 실제로 관련 데이터를 관리하기 위해 온톨로지를 사용할 가능성이 있는 도메인을 사용하면 어떨까?).

OWL을 사용하지 않는 것에 대한 Irene의 블로그 게시물에 대한 가장 눈에 띄는 반발은 암스테르담에 본사를 둔 Triply에서 작성한 [Why We Use OWL Every Day At Triply](https://triply.cc/blog/2021-08-why-we-use-owl)였다. OWL의 가치에 대한 설명은 사람이 읽을 수 있는 모델링 의도 문서로서의 역할에 초점을 맞추었고, 이는 확실히 가치가 있지만 기계가 읽을 수 있는 모델링 지침으로 OWL을 사용하는 것에 대해서는 언급하지 않았다.

저자는 아직 OWL을 가지고 노는 것이 끝나지 않았으며, 다음 핀을 만들어서 적어도 참석자 중 일부가 농담을 할 수 있는 컨퍼런스에 착용하는 꿈을 여전히 꾸고 있다.


<a name="footnote_1">1</a> 이 페이지는 [You probably don't need OWL](https://www.bobdc.com/blog/dontneedowl/)을 편역한 것임.